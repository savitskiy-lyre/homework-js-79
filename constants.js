module.exports = {
   DB_TABLE: {
      locations: 'locations',
      subjects: 'subjects',
      categories: 'categories',
   },
   DB_FIELD: {
      locationID: 'id',
      locationName: 'name',
      locationDescription: 'description',
      categoryID: 'id',
      categoryTitle: 'title',
      categoryDescription: 'description',
      subjectID: 'id',
      subjectCategoryID: 'category_id',
      subjectLocationID: 'location_id',
      subjectTitle: 'title',
      subjectDescription: 'description',
      subjectDatetime: 'creating_time',
      subjectImage: 'image_key',
   },
   PATH: {
      locations: '/locations',
      subjects: '/subjects',
      categories: '/categories',
   },
};