const express = require("express");
const cors = require('cors');
const mysql = require('./db/mysqlDb');
const locations = require('./routes/locations');
const subjects = require('./routes/subjects');
const categories = require('./routes/categories');
const {PATH} = require('./constants');

const server = express();
server.use(express.json());
server.use(cors());
server.use(express.static('public'));

const port = 8080;

server.use(PATH.locations, locations);
server.use(PATH.subjects, subjects);
server.use(PATH.categories, categories);

server.get('/', (req, res) => {
   res.send('Hello');
})

mysql.connect().catch(e => console.log(e));

server.listen(port, () => {
   console.log('Server is started on port ' + port);
})