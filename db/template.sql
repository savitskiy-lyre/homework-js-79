DROP
    database if exists exampleDB;
CREATE
    database if not exists exampleDB;

use
    exampleDB;

create table categories
(
    id          int auto_increment not null PRIMARY KEY,
    title       varchar(255)       not null,
    description text               null
);
create table location
(
    id          int auto_increment not null PRIMARY KEY,
    title       varchar(255)       not null,
    description text               null
);
create table subjects
(
    id            int auto_increment not null PRIMARY KEY,
    title         varchar(255)       not null,
    description   text               null,
    creating_time varchar(255)       not null,
    category_id   int                null,
    location_id   int                null,
    image_key     varchar(255)       null,
    constraint subjects_categories_id_fk
        foreign key (category_id)
            references categories (id)
            on update cascade
            on delete restrict,
    constraint subjects_location_id_fk
        foreign key (location_id)
            references location (id)
            on update cascade
            on delete restrict
);

INSERT INTO categories(title, description)
VALUES ('Food', 'Something tasty'),
       ('Cars', 'Only best cars'),
       ('Books', 'Something funny');
INSERT INTO location(title, description)
VALUES ('Fridge', 'In the lower section'),
       ('Car market', 'In center of our town'),
       ('Store', null);
INSERT INTO subjects(title, description, creating_time, category_id, location_id)
VALUES ('Apple', null, '2020-1-21T22:33:24.706Z', 1, 1),
       ('Honda Fit', '2010 car', '2019-10-01T22:33:24.706Z', 2, 2),
       ('The Three Musketeers', 'funny', '2021-10-31T22:33:24.706Z', 3, 3);

SELECT subjects.title,
       subjects.description,
       categories.title as 'category',
       location.title   as 'location',
       subjects.creating_time
FROM subjects
         inner join location on subjects.location_id = location.id
         inner join categories on subjects.location_id = categories.id;

