const express = require("express");
const route = express.Router();
const mysql = require('../db/mysqlDb');
const {DB_TABLE, DB_FIELD} = require('../constants');

route.get('/', async (req, res) => {
   try {
      const [info] = await mysql.getConnection().query(`SELECT ${DB_FIELD.categoryID + ',' + DB_FIELD.categoryTitle} FROM ${DB_TABLE.categories}`);
      res.send(info);
   } catch (err) {
      res.status(400).send(err.message);
   }
});
route.get('/:id', async (req, res) => {
   if (!req.params.id) return res.status(400).send({err: `The identifier could not be found.`});
   const [info] = await mysql.getConnection().query(`SELECT * FROM ${DB_TABLE.categories} WHERE id = ?`, [req.params.id]);
   res.send(info[0]);
});

route.post('/', async (req, res) => {
   if (!req.body[DB_FIELD.categoryTitle]) return res.status(400).send({error: `Key "${DB_FIELD.categoryTitle}" is undefined`});
   const product = {
      [DB_FIELD.categoryTitle]: req.body[DB_FIELD.categoryTitle],
      [DB_FIELD.categoryDescription]: req.body[DB_FIELD.categoryDescription] ? req.body[DB_FIELD.categoryDescription] : null,
   };
   try {
      const [transient] = await mysql.getConnection().query(
        `INSERT INTO ${DB_TABLE.categories} (${DB_FIELD.categoryTitle + ',' + DB_FIELD.categoryDescription}) values (?, ?)`,
        [product[DB_FIELD.categoryTitle], product[DB_FIELD.categoryDescription]]);
      res.send({
         ...product,
         id: transient.insertId,
      });

   } catch (err) {
      res.status(400).send(err.message);
   }

});

route.delete('/:id', async (req, res) => {
   if (!req.params.id) return res.status(400).send({err: `The identifier could not be found.`});
   try {
      const [transient] = await mysql.getConnection().query(`DELETE FROM ${DB_TABLE.categories} WHERE id=?`, [req.params.id]);
      if (transient.affectedRows === 0) return res.status(404).send({error: "Not Found"});
      res.send({message: 'Deletion done'});
   } catch (err) {
      res.status(400).send(err.message);
   }
});

route.put('/:id', async (req, res) => {
   if (!req.params.id) return res.status(400).send({err: `The identifier could not be found.`});
   if (!req.body[DB_FIELD.categoryTitle]) return res.status(400).send({error: `Key "${DB_FIELD.categoryTitle}" is undefined`});
   const product = {
      [DB_FIELD.categoryTitle]: req.body[DB_FIELD.categoryTitle]
   };
   if (req.body[DB_FIELD.categoryDescription]) product[DB_FIELD.categoryDescription] = req.body[DB_FIELD.categoryDescription];
   try {
      const [transient] = await mysql.getConnection().query(
        `UPDATE ${DB_TABLE.categories} SET ? WHERE id=?`,
        [{...product}, req.params.id]);
      if (transient.affectedRows === 0) return res.status(404).send({error: 'Not found'});
      res.send({...product, id:  req.params.id});
   } catch (err) {
      res.status(400).send(err.message);
   }
});


module.exports = route;