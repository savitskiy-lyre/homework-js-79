const express = require("express");
const route = express.Router();
const mysql = require('../db/mysqlDb');
const {DB_TABLE, DB_FIELD} = require('../constants');

route.get('/', async (req, res) => {
   try {
      const [info] = await mysql.getConnection().query(`SELECT ${DB_FIELD.locationID + ',' + DB_FIELD.locationName} FROM ${DB_TABLE.locations}`);
      res.send(info);
   } catch (err) {
      res.status(400).send(err.message);
   }
});
route.get('/:id', async (req, res) => {
   if (!req.params.id) return res.status(400).send({err: `The identifier could not be found.`});
   const [info] = await mysql.getConnection().query(`SELECT * FROM ${DB_TABLE.locations} WHERE id = ?`, [req.params.id]);
   res.send(info[0]);
});

route.post('/', async (req, res) => {
   if (!req.body.name) return res.status(400).send({error: `Key "${DB_FIELD.locationName}" is undefined`});
   const product = {
      [DB_FIELD.locationName]: req.body[DB_FIELD.locationName],
      [DB_FIELD.locationDescription]: req.body[DB_FIELD.locationDescription] ? req.body[DB_FIELD.locationDescription] : null,
   };
   try {
      const [transient] = await mysql.getConnection().query(
        `INSERT INTO ${DB_TABLE.locations} (${DB_FIELD.locationName + ',' + DB_FIELD.locationDescription}) values (?, ?)`,
        [product[DB_FIELD.locationName], product[DB_FIELD.locationDescription]]);
      res.send({
         ...product,
         id: transient.insertId,
      });

   } catch (err) {
      res.status(400).send(err.message);
   }

});

route.delete('/:id', async (req, res) => {
   if (!req.params.id) return res.status(400).send({err: `The identifier could not be found.`});
   try {
      const [transient] = await mysql.getConnection().query(`DELETE FROM ${DB_TABLE.locations} WHERE id=?`, [req.params.id]);
      if (transient.affectedRows === 0) return res.status(404).send({error: "Not Found"});
      res.send({message: 'Deletion done'});
   } catch (err) {
      res.status(400).send(err.message);
   }
});

route.put('/:id', async (req, res) => {
   if (!req.params.id) return res.status(400).send({err: `The identifier could not be found.`});
   if (!req.body.name) return res.status(400).send({error: `Key "${DB_FIELD.locationName}" is undefined`});
   const product = {
      [DB_FIELD.locationName]: req.body[DB_FIELD.locationName]
   };
   if (req.body[DB_FIELD.locationDescription]) product[DB_FIELD.locationDescription] = req.body[DB_FIELD.locationDescription];
   try {
      const [transient] = await mysql.getConnection().query(
        `UPDATE ${DB_TABLE.locations} SET ? WHERE id=?`,
        [{...product}, req.params.id]);
      if (transient.affectedRows === 0) return res.status(404).send({error: 'Not found'});
      res.send({...product, id:  req.params.id});
   } catch (err) {
      res.status(400).send(err.message);
   }
});


module.exports = route;