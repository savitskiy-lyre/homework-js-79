const express = require("express");
const route = express.Router();
const mysql = require('../db/mysqlDb');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const {DB_TABLE, DB_FIELD} = require('../constants');

const storage = multer.diskStorage({
   destination: (req, file, cb) => {
      cb(null, config.uploadPath);
   },
   filename: (req, file, cb) => {
      cb(null, nanoid() + path.extname(file.originalname));
   }
});

const upload = multer({storage});

route.get('/', async (req, res) => {
   try {
      const [info] = await mysql.getConnection().query(`SELECT ${DB_FIELD.subjectID + ',' + DB_FIELD.subjectTitle} FROM ${DB_TABLE.subjects}`);
      res.send(info);
   } catch (err) {
      res.status(400).send(err.message);
   }
});

route.get('/:id', async (req, res) => {
   if (!req.params.id) return res.status(400).send({err: `The identifier could not be found.`});
   const [info] = await mysql.getConnection().query(`SELECT * FROM ${DB_TABLE.subjects} WHERE id = ?`, [req.params.id]);
   res.send(info[0]);
});

route.post('/', upload.single(DB_FIELD.subjectImage), async (req, res) => {
   if (!req.body[DB_FIELD.subjectTitle] || !req.body[DB_FIELD.subjectCategoryID] || !req.body[DB_FIELD.subjectLocationID]) {
      return res.status(400).send({error: `Data not valid`});
   }
   try {
      const product = {
         [DB_FIELD.subjectTitle]: req.body[DB_FIELD.subjectTitle],
         [DB_FIELD.subjectLocationID]: req.body[DB_FIELD.subjectLocationID],
         [DB_FIELD.subjectCategoryID]: req.body[DB_FIELD.subjectCategoryID],
         [DB_FIELD.subjectDatetime]: new Date().toISOString(),
         [DB_FIELD.subjectImage]: req.file?.filename ? req.file.filename : null,
         [DB_FIELD.subjectDescription]: req.body[DB_FIELD.subjectDescription] ? req.body[DB_FIELD.subjectDescription] : null,
      };
      const [transient] = await mysql.getConnection().query(
        `INSERT INTO ${DB_TABLE.subjects} (??,??,??,??,??,??) values (?,?,?,?,?,?)`,
        [
           DB_FIELD.subjectTitle,
           DB_FIELD.subjectCategoryID,
           DB_FIELD.subjectLocationID,
           DB_FIELD.subjectDatetime,
           DB_FIELD.subjectImage,
           DB_FIELD.subjectDescription,
           product[DB_FIELD.subjectTitle],
           product[DB_FIELD.subjectCategoryID],
           product[DB_FIELD.subjectLocationID],
           product[DB_FIELD.subjectDatetime],
           product[DB_FIELD.subjectImage],
           product[DB_FIELD.subjectDescription]
        ]);
      res.send({
         ...product,
         id: transient.insertId,
      });
   } catch
     (err) {
      res.status(400).send(err.message);
   }

});

route.delete('/:id', async (req, res) => {
   if (!req.params.id) return res.status(400).send({err: `The identifier could not be found.`});
   try {
      const [transient] = await mysql.getConnection().query(`DELETE FROM ${DB_TABLE.subjects} WHERE id=?`, [req.params.id]);
      if (transient.affectedRows === 0) return res.status(404).send({error: "Not Found"});
      res.send({message: 'Deletion done'});
   } catch (err) {
      res.status(400).send(err.message);
   }
});

route.put('/:id', upload.single(DB_FIELD.subjectImage), async (req, res) => {
   if (!req.body[DB_FIELD.subjectTitle] || !req.body[DB_FIELD.subjectCategoryID] || !req.body[DB_FIELD.subjectLocationID]) {
      return res.status(400).send({error: `Data not valid`});
   }
   try {
      const product = {
         [DB_FIELD.subjectTitle]: req.body[DB_FIELD.subjectTitle],
         [DB_FIELD.subjectLocationID]: req.body[DB_FIELD.subjectLocationID],
         [DB_FIELD.subjectCategoryID]: req.body[DB_FIELD.subjectCategoryID],
         [DB_FIELD.subjectDatetime]: new Date().toISOString(),
      };
      if (req.file) product[DB_FIELD.subjectImage] = req.file.filename;
      if (req.body[DB_FIELD.subjectDescription]) product[DB_FIELD.subjectDescription] = req.body[DB_FIELD.subjectDescription];

      const [transient] = await mysql.getConnection().query(
        `UPDATE ${DB_TABLE.subjects} SET ? WHERE id=?`,
        [{...product}, req.params.id]);

      if (transient.affectedRows === 0) return res.status(404).send({error: 'Not found'});
      res.send({...product, id: req.params.id});
   } catch
     (err) {
      res.status(400).send(err.message);
   }
});


module.exports = route;